# RegulatoryCircuits

**Sources:**

_**Input:**_
Input files are similar to those provided by Regulatory Circuits (see Marbach et al., Nature Methods, 2016 for details) but were modified to:
- respect identifiers homogeneity across files;
- remove the comments lines;
- clean and format the headers.

The file _TF_loc.bed_ is an abbreviated version of the original _motif_instances.bed_ file. The lines containing multiple TFs (separated by ':') were duplicated to have one entry for each TF. These locations were made unique by taking the max confidence score for several occurrences of the same TF binding site (TFBS) (same TF, exact same genomic location [chr:start-end]). All lines for which the confidence score was 0 were removed to reduce the size of the file (as they should not be taken into account for the networks computation).

The following files are provided:
- **gene_coord.bed**: genomic coordinates of genes according to Ensembl reference (as of May 31st, 2014).
- **hg19.cage_peak_coord_robust.bed**: genomic coordinates of promoters from FANTOM5.
- **hg19_cage_peak_OK.txt**: raw expression data of promoters from FANTOM5 (changer le nom ?).
- **hg19_permissive_enhancers_expression_OK.csv**: raw expression data of enhancers from FANTOM5.
- **permissive_enhancers.bed**: genomic coordinates peak calling values of enhancers from FANTOM5.
- **TF_loc.bed**: TFBS genomic locations from manually curated TFBS (see Marbach et al., 2016 for details and references).
- **transcript_coord.bed**: genomic coordinates of transcripts according to Ensembl reference (as of May 31st, 2014).
- **dictonary_Sample_Tissue.tsv**: list of the samples to tissues correspondances, using _libId_ & _Fantom5 ID_ for the samples.

_**Intermediary:**_
As for the input files, intermediary files are modified versions of the ones provided by Regulatory Circuits. Most of those files can be re-computed using the scripts described in the script section. _Enhancer-Rank.txt_ and _Promoter-Rank.txt_ are already recomputed versions of the ranks, according to the methodology described in the _methods_ section of Marbach et al., 2016.

The following files are provided:
- **Enhancer-Rank.txt**: expression ranks of enhancers (recomputed following the published method).
- **enhancer---transcript.txt**: relations and distance weights between enhancers and transcripts.
- **Promoter-Rank.txt**: expression ranks of promoters (recomputed following the published method).
- **promoter---transcript.txt**: relations between promoters and transcripts.
- **tf---enhancer.txt**: TFBS occurrences and max confidence scores in enhancers (one TF - one enhancer relation).
- **tf---promoter.csv**: TFBS occurrences and max confidence scores in promoters (one TF - one promoter relation).
- **transcript---gene.txt**: relations between transcripts and genes.



**Scripts:**

- **CodeRank.R** : recomputes the rank/normalization of the regions (enhancers and promoters) using the published methodology, produces the files _Promoter-Rank.txt_ & _Enhancer-Rank.txt_
- **distance.py** : uses the coordinates of the transcripts and the regions to compute the distance between those entities
- **extract_transcriptRANK.sh** : extracts the ranks of all transcripts in a given sample using _Promoter-Rank.txt_ and _promoter---transcript.txt_ as basis. Is called in _pipeline_enhancer.sh_
- **intersect_TF.sh** : computes the intersections between the TFBS and the regions, produces the files _tf---enhancer.txt_ & _tf---promoter.csv_
- **network_samples.sh** : computes the sample-specific networks, use both _pipeline_enhancer.sh_ & _pipeline_promoter.sh_ and only give the max of weightP and weightE as final score
- **pipeline_enhancer.sh** : computes the TF-genes relations for a sample (given as argument) only using the enhancers. Use _extract_transcriptRANK.sh_ .
- **pipeline_promoter.sh** : computes the TF-genes relations for a sample (given as argument) only using the promoters.
- **regression_R.R** : recover the formula used to obtain the weight of the distance for the enhancers. Use _enhancer---transcript.txt_ as entry.
- **samples_to_tissue.sh** : computes the tissue-specific networks by taking a union of the networks computed for the samples constituting the tissue. In case of redundant TF-gene relations, the score given in the resulting networks is the max of the TF-genes relations across all samples.Must be run after _network_samples.sh_.



**Validation:**
- **RC original networks** : contains the 12 networks used in the validation step, extracted from the published networks of Regulatory Circuits, format: TF / gene / score.
- **Re-computed Networks** : contains the 12 recomputed networks using the provided _bash_ workflow, format: TF / gene / score.
- **RNA-seq.csv** : contains the RNAseq expression of 18,623 genes across the 12 tissues used for the networks validations.
