import pandas as pd
import csv

table_enhancer = pd.read_csv('../sources/input/permissive_enhancers.bed',delimiter='\t', header=None)
table_promoter = pd.read_csv('../sources/input/hg19.cage_peak_coord_robust.bed',delimiter='\t', header=None)
table_transcript = pd.read_csv('../sources/input/transcript_coord.bed',delimiter='\t', header=None)

#enhancer: For enhancer the distance are limited to |500kb|
with open('../output/enhancer---transcript.txt', 'w') as transcript-enh:
	for i in range(0,len(table_enhancer[0])) :
		table_transcript_chrom = table_transcript[table_transcript[0] == table_enhancer.iloc[i][0]]
		for j in range(0,len(table_transcript_chrom[0])) :
			if (table_enhancer.iloc[i][1] >= table_transcript_chrom.iloc[j][1] and table_enhancer.iloc[i][1] <= table_transcript_chrom.iloc[j][2]) or (table_enhancer.iloc[i][2] >= table_transcript_chrom.iloc[j][1] and table_enhancer.iloc[i][2] <= table_transcript_chrom.iloc[j][2]):
				distance = 0
			elif table_enhancer.iloc[i][1] < table_transcript_chrom.iloc[j][1] :
				distance = table_transcript_chrom.iloc[j][1] - table_enhancer.iloc[i][2] + 1
			else :
				distance = table_transcript_chrom.iloc[j][2] - table_enhancer.iloc[i][1] + 1
			if distance <= 500000 and distance >= -500000 :
				transcript-enh.write(table_enhancer.iloc[i][3] + '\t' + table_transcript_chrom.iloc[j][3] +'\t' + str(distance))


#promoter: The limit of the distance was found in the intermediary files
with open('../output/promoter---transcript.txt', 'w') as transcript-pro:
	for i in range(0,len(table_promoter[0])) :
		table_transcript_chrom = table_transcript[table_transcript[0] == table_promoter.iloc[i][0]]
		for j in range(0,len(table_transcript_chrom[0])) :
			if (table_promoter.iloc[i][1] >= table_transcript_chrom.iloc[j][1] and table_promoter.iloc[i][1] <= table_transcript_chrom.iloc[j][2]) or (table_promoter.iloc[i][2] >= table_transcript_chrom.iloc[j][1] and table_promoter.iloc[i][2] <= table_transcript_chrom.iloc[j][2]):
				distance = 0
			elif table_promoter.iloc[i][1] < table_transcript_chrom.iloc[j][1] :
				distance = table_transcript_chrom.iloc[j][1] - table_promoter.iloc[i][2] + 1
			else :
				distance = table_transcript_chrom.iloc[j][2] - table_promoter.iloc[i][1] + 1
			if distance <= 500 and distance >= -250 :
				transcript-pro.write(table_promoter.iloc[i][3] + '\t' + table_transcript_chrom.iloc[j][3] +'\t' + str(distance))

