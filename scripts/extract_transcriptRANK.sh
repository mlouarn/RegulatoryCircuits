#!/bin/bash

function usage() {
    echo "Usage: $0 <R/A> <sample>"
}

if (( $# != 1 )); then
    usage >&2
    exit -1
fi


sample=$1
tmp=$(mktemp -d)
mkdir -p tmp_files

#extract column 1 (promoter name) and i coresponding of the sample
awkscript='{
    if(NR == 1) {
        for(i = 1; i <= NF; ++i) {
            if($i == "'$sample'") {
                col = i;
            }
        }
    }
    else if($col != 0) {
        print $1, $col;
    }
}'

awk "$awkscript" ../sources/intermediary/Promoter-Rank.csv > $tmp/trans_step1.txt

#get the relation between transcript and promoter
sort -k1,1 $tmp/trans_step1.txt > $tmp/trans_step2.txt
sort -k1,1 ../sources/intermediary/promoter---transcript.txt > $tmp/trans_step3.txt
join $tmp/trans_step2.txt $tmp/trans_step3.txt > $tmp/trans_step4.txt
sed -i 's/ /\t/g' $tmp/trans_step4.txt #necessary for groupBy

#sort by promoter
sort -k4 $tmp/trans_step4.txt > $tmp/trans_step5.txt
groupBy -i $tmp/trans_step5.txt -g 4 -c 2 -o max > tmp_files/transcript_Rank_$sample.txt

rm $tmp/*.txt
rmdir $tmp
