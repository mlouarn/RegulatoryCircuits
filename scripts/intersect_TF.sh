#!/bin/bash

tmp=$(mktemp -d)

#promoter:
bedtools intersect -wa -wb -F 1 -a ../sources/input/hg19.cage_peak_coord_robust.bed -b ../sources/input/TF_loc.bed > $tmp/step1.txt
awk '{print $4,$12,$13}' $tmp/step1.txt > $tmp/step2.txt
sort k-1,2 $tmp/step2.txt > $tmp/step3.txt
groupBy -i $tmp/step3.txt -g 1,2 -c 3 -o max > ../output/tf---promoter.csv

#enhancer:
bedtools intersect -wa -wb -F 1 -a ../sources/input/permissive_enhancers.bed -b ../sources/input/TF_loc.bed > $tmp/step1.txt
awk '{print $4,$12,$13}' $tmp/step1.txt > $tmp/step2.txt
sort k-1,2 $tmp/step2.txt > $tmp/step3.txt
groupBy -i $tmp/step3.txt -g 1,2 -c 3 -o max > ../output/tf---enhancer.csv

rm $tmp/*.txt
rmdir $tmp
