#!/bin/bash
function usage() {
    echo "Usage: $0<R/A> <sample>"
}

if (( $# != 1 )); then
    usage >&2
    exit -1
fi

sample=$1

tmp=$(mktemp -d)
mkdir -p tmp_files


#extract column 1 (promoter name) and i coresponding of the sample
awkscript='{
    if(NR == 1) {
	for(i = 1; i <= NF; ++i) {
	    if($i == "'$sample'") {
	        col = i;
	    }
	}
    }
    else if($col != 0) {
	print $1, $col;
    }
}'
awk "$awkscript" ../sources/intermediary/Enhancer-Rank.csv > $tmp/enh_step1.txt

#relation between enhancer-TF
sort -k1,1 $tmp/enh_step1.txt > $tmp/enh_step2.txt 
sort -k1,1 ../sources/intermediary/tf---enhancer.txt > $tmp/enh_step3.txt
join $tmp/enh_step3.txt $tmp/enh_step2.txt > $tmp/enh_step4.txt
 
#relation between enhancer-trancript-tf
sort -k1,1 ../sources/intermediary/enhancer---transcript.txt > $tmp/enh_step5.txt
join $tmp/enh_step5.txt $tmp/enh_step4.txt > $tmp/enh_tf---enhancer.txt

#change order to transcript enhancer distance weight gene tf confidence rank
awk '{ print $2"\t"$1"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8}' $tmp/enh_tf---enhancer.txt > $tmp/enh_step6.txt
sort -k1,1 $tmp/enh_step6.txt > $tmp/enh_step7.txt

#Obtain the rank of the transcript
./extract_transcriptRANK.sh $sample
sort -k1,1 tmp_files/transcript_Rank_$sample.txt > $tmp/enh_step9.txt
join $tmp/enh_step9.txt $tmp/enh_step7.txt > $tmp/enh_step10.txt

#calculate weight of relation entities : tf,gene,transcript,enhancer,weight
awk '{print $7"\t"$6"\t"$8*$5*sqrt($9*$2)}' $tmp/enh_step10.txt > $tmp/enh_step11.txt
sort -k1,2 $tmp/enh_step11.txt > $tmp/enh_step12.txt
groupBy -i $tmp/enh_step12.txt -g 1,2 -c 3 -o max > $tmp/enh_step13.txt
sed -i '1 i\tf\tgene\tscore' $tmp/enh_step13.txt
awk '$3 != 0{print}' $tmp/enh_step13.txt > tmp_files/Enhancer_$sample.txt


rm $tmp/*.txt
rmdir $tmp
