#!/bin/bash
function usage() {
    echo "Usage: $0<R/A> <sample>"
}

if (( $# != 1 )); then
    usage >&2
    exit -1
fi

sample=$1
tmp=$(mktemp -d)
mkdir -p tmp_files

#extract column 1 (promoter name) and i coresponding of the sample
awkscript='{
    if(NR == 1) {
	for(i = 1; i <= NF; ++i) {
	    if($i == "'$sample'") {
	        col = i;
	    }
	}
    }
    else if($col != 0) {
	print $1, $col;
    }
}'

awk "$awkscript" ../sources/intermediary/Promoter-Rank.txt > $tmp/pro_step1.txt


#sort promoter
sort -k1,1 $tmp/pro_step1.txt > $tmp/pro_step2.txt 
sort -k1,1 ../sources/intermediary/tf---promoter.csv > $tmp/pro_step3.txt
join $tmp/pro_step3.txt $tmp/pro_step2.txt > $tmp/pro_step4.txt

sort -k1,1 ../sources/intermediary/promoter---transcript.txt > $tmp/pro_step5.txt
join $tmp/pro_step5.txt $tmp/pro_step4.txt > $tmp/pro_tf---promoter.txt

awk '{print $6"\t"$5"\t"$7*$8}' $tmp/pro_tf---promoter.txt > $tmp/pro_step6.txt
sort -k1,2 $tmp/pro_step6.txt > $tmp/pro_step7.txt
groupBy -i $tmp/pro_step7.txt -g 1,2 -c 3 -o max > $tmp/pro_step8.txt
sed -i '1 i\tf\tgene\tscore' $tmp/pro_step8.txt
awk '$3 != 0{print}' $tmp/pro_step8.txt > tmp_files/Promoter_$sample.txt

rm $tmp/*.txt
rmdir $tmp
