#!/bin/bash

tmp=$(mktemp -d)
mkdir -p ../Networks

t=(acute_lymphoblastic_leukemia-B-ALL-cell_line:CNhs11251:CNhs11282
acute_lymphoblastic_leukemia-T-ALL-cell_line:CNhs10746:CNhs11253
acute_myeloid_leukemia-FAB_M4-cell_line:CNhs13503:CNhs13504
acute_myeloid_leukemia-FAB_M4eo-cell_line:CNhs13056:CNhs13057
acute_myeloid_leukemia-FAB_M7-cell_line:CNhs11888:CNhs13049
Adipocyte-breast:CNhs11051:CNhs11969
Alveolar_Epithelial_Cells:CNhs11325:CNhs12084
Amniotic_Epithelial_Cells:CNhs11341:CNhs12125
amygdala-adult:CNhs13793:CNhs12311
Anulus_Pulposus_Cell:CNhs10876:CNhs12064
bile_duct_carcinoma_cell_line:CNhs10750:CNhs11265
brain-adult:CNhs11796:CNhs10617
breast_carcinoma_cell_line:CNhs11943:CNhs10736
Burkitts_lymphoma_cell_line:CNhs10739:CNhs11268
carcinoid_cell_line:CNhs11834:CNhs11846
caudate_nucleus-adult:CNhs13802:CNhs12321
CD14-CD16+_Monocytes:CNhs13207:CNhs13548
CD4+CD25-CD45RA-_memory_conventional_T_cells:CNhs13215:CNhs13539
CD4+CD25+CD45RA+_naive_regulatory_T_cells:CNhs13203:CNhs13513
cervical_cancer_cell_line:CNhs11288:CNhs11289
Chondrocyte-re_diff:CNhs11373:CNhs12021
clear_cell_carcinoma_cell_line:CNhs11745:CNhs11930
colon-adult:CNhs11794:CNhs10619
colon_carcinoma_cell_line:CNhs11280:CNhs10737
Corneal_Epithelial_Cells:CNhs11336:CNhs12123
ductal_cell_carcinoma_cell_line:CNhs11100:CNhs11259
duodenum-fetal:CNhs11781:CNhs12997
embryonic_kidney_cell_line:CNhs11047:CNhs11046
Endothelial_Cells-Thoracic:CNhs11926:CNhs11978
epidermoid_carcinoma_cell_line:CNhs10743:CNhs10748
Fibroblast-Aortic_Adventitial:CNhs10874:CNhs12011
Fibroblast-Choroid_Plexus:CNhs11319:CNhs12344
Fibroblast-Lymphatic:CNhs11322:CNhs12118
Fibroblast-skin_normal:CNhs11351:CNhs11914
gall_bladder_carcinoma_cell_line:CNhs11256:CNhs10733
giant_cell_carcinoma_cell_line:CNhs11274:CNhs10751
globus_pallidus-adult:CNhs13801:CNhs12319
Hair_Follicle_Outer_Root_Sheath_Cells:CNhs12339:CNhs12347
Hepatic_Sinusoidal_Endothelial_Cells:CNhs12075:CNhs12092
Hepatic_Stellate_Cells-lipocyte:CNhs11335:CNhs12093
hippocampus-adult:CNhs13795:CNhs12312
Keratocytes:CNhs11337:CNhs12095
langerhans_cells-immature:CNhs13537:CNhs13480
large_cell_lung_carcinoma_cell_line:CNhs11277:CNhs12806
liposarcoma_cell_line:CNhs11870:CNhs11851
locus_coeruleus-adult:CNhs13808:CNhs12322
lung_adenocarcinoma_cell_line:CNhs10726:CNhs11275
Mallassez-derived_cells:CNhs13550:CNhs13551
medial_frontal_gyrus-adult:CNhs13796:CNhs12310
medulloblastoma_cell_line:CNhs12805:CNhs11861
melanoma_cell_line:CNhs11281:CNhs11254
merkel_cell_carcinoma_cell_line:CNhs12838:CNhs12839
Mesenchymal_stem_cells-adipose:CNhs11345:CNhs12922
Mesenchymal_stem_cells-amniotic_membrane:CNhs11349:CNhs12104
Mesenchymal_stem_cells-hepatic:CNhs10845:CNhs12730
Mesothelial_Cells:CNhs10850:CNhs12012
mucinous_adenocarcinoma_cell_line:CNhs11752:CNhs11873
Multipotent_Cord_Blood_Unrestricted_Somatic_Stem_Cells:CNhs11350:CNhs12105
myxofibrosarcoma_cell_line:CNhs11729:CNhs11821
nasal_epithelial_cells:CNhs12589:CNhs12574
Neural_stem_cells:CNhs11063:CNhs11384
occipital_cortex-adult:CNhs13798:CNhs12320
Osteoblast:CNhs11385:CNhs12036
osteosarcoma_cell_line:CNhs11279:CNhs11290
Pericytes:CNhs11317:CNhs12079
pineal_gland-adult:CNhs13804:CNhs12228
pituitary_gland-adult:CNhs13805:CNhs12229
Preadipocyte-breast:CNhs11052:CNhs11971
Preadipocyte-subcutaneous:CNhs11981:CNhs12038
prostate_cancer_cell_line:CNhs11260:CNhs11243
renal_cell_carcinoma_cell_line:CNhs10729:CNhs11257
Renal_Cortical_Epithelial_Cells:CNhs11331:CNhs12728
Renal_Mesangial_Cells:CNhs11333:CNhs12121
Reticulocytes:CNhs13552:CNhs13553
rhabdomyosarcoma_cell_line:CNhs11269:CNhs11877
schwannoma_cell_line:CNhs11183:CNhs11245
Sebocyte:CNhs10847:CNhs11951
signet_ring_carcinoma_cell_line:CNhs10753:CNhs11270
small_cell_gastrointestinal_carcinoma_cell_line:CNhs11734:CNhs11736
Smooth_Muscle_Cells-Brachiocephalic:CNhs11086:CNhs12043
Smooth_Muscle_Cells-Bronchial:CNhs11328:CNhs12348
Smooth_Muscle_Cells-Carotid:CNhs11087:CNhs12044
Smooth_Muscle_Cells-Internal_Thoracic_Artery:CNhs11988:CNhs12046
Smooth_Muscle_Cells-Prostate:CNhs11920:CNhs11976
Smooth_Muscle_Cells-Tracheal:CNhs11329:CNhs12894
Smooth_Muscle_Cells-Umbilical_Vein:CNhs12597:CNhs12569
spinal_cord-adult:CNhs13807:CNhs12227
squamous_cell_carcinoma_cell_line:CNhs11252:CNhs11739
Synoviocyte:CNhs11992:CNhs12050
temporal_lobe-fetal:CNhs12996:CNhs11772
testis-adult:CNhs10632:CNhs12998
thalamus-adult:CNhs13794:CNhs12314
Trabecular_Meshwork_Cells:CNhs11340:CNhs12124
transitional-cell_carcinoma_cell_line:CNhs10735:CNhs11261
Wilms_tumor_cell_line:CNhs11892:CNhs11728)


for x in ${t[*]}; do
    T=$(echo ${x} | awk -F: '{print $1}')
    A=$(echo ${x} | awk -F: '{print $2}')
    B=$(echo ${x} | awk -F: '{print $3}')
    sed '1d' ../networks_Samples/${A}.tsv > $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${B}.tsv >> $tmp/${T}_1.tsv
    sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
    bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max > ../Networks/${T}.tsv
    sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv
done

t=(acute_myeloid_leukemia-FAB_M2-cell_line:CNhs13502:CNhs13052:CNhs11864
acute_myeloid_leukemia-FAB_M6-cell_line:CNhs13059:CNhs13060:CNhs13505
Adipocyte-omental:CNhs11054:CNhs12067:CNhs12068
Adipocyte-subcutaneous:CNhs12494:CNhs11371:CNhs12017
amniotic_membrane_cells:CNhs12502:CNhs12503:CNhs12379
Astrocyte-cerebellum:CNhs11321:CNhs12081:CNhs12117
Astrocyte-cerebral_cortex:CNhs10864:CNhs11960:CNhs12005
B_lymphoblastoid_cell_line:CNhs12331:CNhs12332:CNhs12333
Bronchial_Epithelial_Cell:CNhs12054:CNhs12058:CNhs12062
Cardiac_Myocyte:CNhs12341:CNhs12350:CNhs12571
CD14+CD16-_Monocytes:CNhs13224:CNhs13216:CNhs13540
CD14+CD16+_Monocytes:CNhs13541:CNhs13208:CNhs13549
CD19+_B_Cells:CNhs12343:CNhs12352:CNhs12354
CD4+_T_Cells:CNhs10853:CNhs11955:CNhs11998
CD8+_T_Cells:CNhs10854:CNhs11956:CNhs11999
cerebellum-adult:CNhs13799:CNhs12323:CNhs11795
Chondrocyte-de_diff:CNhs11923:CNhs11372:CNhs12020
choriocarcinoma__cell_line:CNhs11820:CNhs10740:CNhs11875
chorionic_membrane_cells:CNhs12504:CNhs12506:CNhs12380
Ciliary_Epithelial_Cells:CNhs10871:CNhs11966:CNhs12009
cord_blood_derived_cell_line:CNhs11050:CNhs11049:CNhs11045
Dendritic_Cells-monocyte_immature_derived:CNhs10855:CNhs11062:CNhs12000
endometrial_cancer_cell_line:CNhs11266:CNhs11249:CNhs11748
Endothelial_Cells-Artery:CNhs12496:CNhs11977:CNhs12023
Endothelial_Cells-Lymphatic:CNhs10865:CNhs11901:CNhs11906
Endothelial_Cells-Microvascular:CNhs11925:CNhs11376:CNhs12024
Endothelial_Cells-Umbilical_vein:CNhs10872:CNhs11967:CNhs12010
Endothelial_Cells-Vein:CNhs12497:CNhs11377:CNhs12026
Endothelial_Progenitor_Cells:CNhs10858:CNhs11897:CNhs11904
epitheloid_cancer_cell_line:CNhs12325:CNhs12326:CNhs12327
Fibroblast-Periodontal_Ligament-adult:CNhs12493:CNhs11953:CNhs11996
Fibroblast-Periodontal_Ligament-fetal:CNhs10867:CNhs11962:CNhs11907
Fibroblast-skin_dystrophia_myotonica:CNhs11353:CNhs11354:CNhs11913
Fibroblast-skin_spinal_muscular_atrophy:CNhs11074:CNhs11911:CNhs11912
Gingival_epithelial_cells:CNhs11061:CNhs11896:CNhs11903
Hair_Follicle_Dermal_Papilla_Cells:CNhs12501:CNhs11979:CNhs12030
heart-adult:CNhs11757:CNhs11758:CNhs10621
Hepatocyte:CNhs12340:CNhs12349:CNhs12626
Keratinocyte-epidermal:CNhs11064:CNhs11381:CNhs12031
langerhans_cells-migratory:CNhs13535:CNhs13536:CNhs13547
leiomyoma_cell_line:CNhs11722:CNhs11723:CNhs11724
Lens_Epithelial_Cells:CNhs12342:CNhs12568:CNhs12572
Macrophage-monocyte_derived:CNhs10861:CNhs11899:CNhs12003
Mammary_Epithelial_Cell:CNhs11077:CNhs11382:CNhs12032
medulla_oblongata-adult:CNhs13800:CNhs12315:CNhs10645
Meningeal_Cells:CNhs11320:CNhs12080:CNhs12731
mesenchymal_precursor_cell-adipose:CNhs12363:CNhs12364:CNhs12365
mesenchymal_precursor_cell-bone_marrow:CNhs12366:CNhs12367:CNhs13098
Mesenchymal_stem_cells-bone_marrow:CNhs11344:CNhs12100:CNhs12126
Mesenchymal_stem_cells-umbilical:CNhs12492:CNhs11347:CNhs12127
Myoblast:CNhs10870:CNhs11965:CNhs11908
Natural_Killer_Cells:CNhs10859:CNhs11957:CNhs12001
neuroectodermal_tumor_cell_line:CNhs11866:CNhs11744:CNhs11753
Neurons:CNhs12338:CNhs12726:CNhs13815
Neutrophils:CNhs10862:CNhs11959:CNhs11905
Nucleus_Pulposus_Cell:CNhs10881:CNhs12019:CNhs12063
Osteoblast-differentiated:CNhs11311:CNhs11980:CNhs12035
parietal_lobe-adult:CNhs13797:CNhs12317:CNhs10641
Peripheral_Blood_Mononuclear_Cells:CNhs10860:CNhs11958:CNhs12002
Placental_Epithelial_Cells:CNhs11079:CNhs11386:CNhs12037
Preadipocyte-omental:CNhs11065:CNhs11902:CNhs12013
Preadipocyte-visceral:CNhs11082:CNhs11982:CNhs12039
Prostate_Epithelial_Cells:CNhs11972:CNhs12014:CNhs10882
Prostate_Stromal_Cells:CNhs10883:CNhs11973:CNhs12015
Renal_Epithelial_Cells:CNhs11332:CNhs12088:CNhs12732
Renal_Proximal_Tubular_Epithelial_Cell:CNhs11330:CNhs12087:CNhs12120
Retinal_Pigment_Epithelial_Cells:CNhs10842:CNhs11338:CNhs12733
salivary_acinar_cells:CNhs12810:CNhs12811:CNhs12812
serous_adenocarcinoma_cell_line:CNhs11746:CNhs13508:CNhs13099
Skeletal_Muscle_Satellite_Cells:CNhs10869:CNhs11964:CNhs12008
Small_Airway_Epithelial_Cells:CNhs10884:CNhs11975:CNhs12016
Smooth_Muscle_Cells-Brain_Vascular:CNhs10863:CNhs11900:CNhs12004
Smooth_Muscle_Cells-Colonic:CNhs10868:CNhs11963:CNhs12007
Smooth_Muscle_Cells-Coronary_Artery:CNhs11088:CNhs11987:CNhs12045
Smooth_Muscle_Cells-Subclavian_Artery:CNhs11090:CNhs11990:CNhs12048
tenocyte:CNhs12639:CNhs12640:CNhs12641
Tracheal_Epithelial_Cells:CNhs11092:CNhs11993:CNhs12051
teratocarcinoma_cell_line:CNhs11890:CNhs11878:CNhs11884)


for x in ${t[*]}; do
    T=$(echo ${x} | awk -F: '{print $1}')
    A=$(echo ${x} | awk -F: '{print $2}')
    B=$(echo ${x} | awk -F: '{print $3}')
    C=$(echo ${x} | awk -F: '{print $4}')

    sed '1d' ../networks_Samples/${A}.tsv > $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${B}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${C}.tsv >> $tmp/${T}_1.tsv
    sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
    bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max > ../Networks/${T}.tsv
    sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv
done


t=(embryonic_pancreas_cell_line:CNhs11731:CNhs11732:CNhs11733:CNhs11814
Endothelial_Cells-Aortic:CNhs10837:CNhs12495:CNhs11375:CNhs12022
gastric_cancer_cell_line:CNhs11737:CNhs11819:CNhs11286:CNhs11738
glioma_cell_line:CNhs11185:CNhs11248:CNhs11272:CNhs10731
hepatocellular_carcinoma_cell_line:CNhs12328:CNhs12329:CNhs12330:CNhs11271
Melanocyte:CNhs12570:CNhs11303:CNhs11383:CNhs12033
mesenchymal_precursor_cell-cardiac:CNhs12368:CNhs12369:CNhs12370:CNhs12371
mesenchymal_precursor_cell-ovarian_cancer_left_ovary:CNhs12372:CNhs13092:CNhs12376:CNhs13094
mesenchymal_precursor_cell-ovarian_cancer_metastasis:CNhs12374:CNhs13093:CNhs12378:CNhs13097
neuroblastoma_cell_line:CNhs11276:CNhs11284:CNhs11818:CNhs11811
Olfactory_epithelial_cells:CNhs13816:CNhs13817:CNhs13818:CNhs13819
oral_squamous_cell_carcinoma_cell_line:CNhs10752:CNhs11287:CNhs11717:CNhs11810
Renal_Glomerular_Endothelial_Cells:CNhs13080:CNhs12074:CNhs12086:CNhs12624
Skeletal_Muscle_Cells:CNhs11083:CNhs12053:CNhs12056:CNhs12060
small_cell_lung_carcinoma_cell_line:CNhs12808:CNhs11285:CNhs12809:CNhs11812
Smooth_Muscle_Cells-Aortic:CNhs10838:CNhs11085:CNhs11305:CNhs11309
Smooth_Muscle_Cells-Umbilical_Artery:CNhs10839:CNhs11091:CNhs11991:CNhs12049
testicular_germ_cell_embryonal_carcinoma_cell_line:CNhs11876:CNhs12351:CNhs12362:CNhs11726
Urothelial_Cells:CNhs11334:CNhs12091:CNhs12122:CNhs10843)


for x in ${t[*]}; do
    T=$(echo ${x} | awk -F: '{print $1}')
    A=$(echo ${x} | awk -F: '{print $2}')
    B=$(echo ${x} | awk -F: '{print $3}')
    C=$(echo ${x} | awk -F: '{print $4}')
    D=$(echo ${x} | awk -F: '{print $5}')

    sed '1d' ../networks_Samples/${A}.tsv > $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${B}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${C}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${D}.tsv >> $tmp/${T}_1.tsv
    sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
    bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max > ../Networks/${T}.tsv
    sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv
done

t=(Fibroblast-Gingival:CNhs10866:CNhs11961:CNhs12006:CNhs10848:CNhs11952
Mast_cell:CNhs11073:CNhs12566:CNhs12594:CNhs12593:CNhs12592
mesenchymal_precursor_cell-ovarian_cancer_right_ovary:CNhs12373:CNhs12375:CNhs12377:CNhs13507:CNhs13096)


for x in ${t[*]}; do
    T=$(echo ${x} | awk -F: '{print $1}')
    A=$(echo ${x} | awk -F: '{print $2}')
    B=$(echo ${x} | awk -F: '{print $3}')
    C=$(echo ${x} | awk -F: '{print $4}')
    D=$(echo ${x} | awk -F: '{print $5}')
    E=$(echo ${x} | awk -F: '{print $6}')

    sed '1d' ../networks_Samples/${A}.tsv > $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${B}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${C}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${D}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${E}.tsv >> $tmp/${T}_1.tsv
    sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
    bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max >../Networks/${T}.tsv
    sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv
done

t=(acute_myeloid_leukemia-FAB_M5-cell_line:CNhs13050:CNhs13051:CNhs10722:CNhs10723:CNhs10724:CNhs13058
CD4+CD25+CD45RA-_memory_regulatory_T_cells:CNhs13204:CNhs13811:CNhs13812:CNhs13195:CNhs13206:CNhs13538
CD4+CD25-CD45RA+_naive_conventional_T_cells:CNhs13202:CNhs13813:CNhs13814:CNhs13223:CNhs13205:CNhs13512
Fibroblast-Cardiac:CNhs12498:CNhs11378:CNhs12027:CNhs11909:CNhs12057:CNhs12061
Fibroblast-Dermal:CNhs12499:CNhs11379:CNhs12028:CNhs12052:CNhs12055:CNhs12059)


for x in ${t[*]}; do
    T=$(echo ${x} | awk -F: '{print $1}')
    A=$(echo ${x} | awk -F: '{print $2}')
    B=$(echo ${x} | awk -F: '{print $3}')
    C=$(echo ${x} | awk -F: '{print $4}')
    D=$(echo ${x} | awk -F: '{print $5}')
    E=$(echo ${x} | awk -F: '{print $6}')
    F=$(echo ${x} | awk -F: '{print $7}')

    sed '1d' ../networks_Samples/${A}.tsv > $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${B}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${C}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${D}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${E}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${F}.tsv >> $tmp/${T}_1.tsv
    sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
    bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max > ../Networks/${T}.tsv
    sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv
done

t=(chronic_myelogenous_leukemia-CML-cell_line:CNhs11250:CNhs12334:CNhs12335:CNhs12336:CNhs11886:CNhs10727:CNhs11865)
for x in ${t[*]}; do
    T=$(echo ${x} | awk -F: '{print $1}')
    A=$(echo ${x} | awk -F: '{print $2}')
    B=$(echo ${x} | awk -F: '{print $3}')
    C=$(echo ${x} | awk -F: '{print $4}')
    D=$(echo ${x} | awk -F: '{print $5}')
    E=$(echo ${x} | awk -F: '{print $6}')
    F=$(echo ${x} | awk -F: '{print $7}')
    G=$(echo ${x} | awk -F: '{print $8}')

    sed '1d' ../networks_Samples/${A}.tsv > $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${B}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${C}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${D}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${E}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${F}.tsv >> $tmp/${T}_1.tsv
    echo -e '\n' >> $tmp/${T}_1.tsv
    sed '1d' ../networks_Samples/${G}.tsv >> $tmp/${T}_1.tsv
    sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
    bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max >../Networks/${T}.tsv
    sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv
done

T='Whole_blood-ribopure'
sed '1d' ../networks_Samples/CNhs11675.tsv > $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11671.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11948.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11075.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11076.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11672.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11673.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11949.tsv >> $tmp/${T}_1.tsv
sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max >../Networks/${T}.tsv
sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv

T='Hep-2_cells'
sed '1d' ../networks_Samples/CNhs13479.tsv > $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13500.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13501.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13477.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13496.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13497.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13478.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13498.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13499.tsv >> $tmp/${T}_1.tsv
sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max >../Networks/${T}.tsv
sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv

T='mesothelioma_cell_line'
sed '1d' ../networks_Samples/CNhs11263.tsv > $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11264.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13066.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13067.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13068.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13069.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13070.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13072.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13073.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13063.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13062.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13064.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13061.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13074.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13075.tsv >> $tmp/${T}_1.tsv
sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max >../Networks/${T}.tsv
sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv


T='CD14+_Monocytes'
sed '1d' ../networks_Samples/CNhs10852.tsv > $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11954.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs11997.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13468.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13484.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13491.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13465.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13475.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13543.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13474.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13483.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13489.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13495.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13473.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13488.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13494.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13472.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13487.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13546.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13469.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13532.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13492.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13466.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13476.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13490.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13470.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13533.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13545.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13471.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13485.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13493.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13467.tsv >> $tmp/${T}_1.tsv
echo -e '\n' >> $tmp/${T}_1.tsv
sed '1d' ../networks_Samples/CNhs13544.tsv >> $tmp/${T}_1.tsv
sort -k1,2 $tmp/${T}_1.tsv > $tmp/${T}_2.tsv
bedtools groupby -i $tmp/${T}_2.tsv -g 1,2 -c 3 -o max >../Networks/${T}.tsv
sed -i '1 i\TF\tGene\tscore' ../Networks/${T}.tsv

rm $tmp/*.tsv
rmdir $tmp
